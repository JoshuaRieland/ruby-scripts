array = [3,5,1,2,7,9,8,13,25,32]

puts ''
puts 'Print all items in array'
puts array

puts ''
puts 'Print all values in array over 10'
_something = array.select { |val| val > 10 }
puts _something

puts ''
puts 'Print sum of all values in array'
sum = 0
array.each { |val| sum+=val}
puts sum

puts ''
puts 'Shuffle Alphabet'
alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
alphabet.shuffle!
puts alphabet

puts ''
puts 'Shuffle Names Array'
names = ['John', 'KB', 'Oliver', 'Cory', 'Mathew', 'Christopher']
names.shuffle!
puts names

puts ''
puts 'Return Names > 5 Characters'
longNames = []
longNames = names.select {|val| val.length > 5 }
puts longNames

puts ''
puts 'Random Array with numbers between 55 and 100'
randInt = []
start = 55
ender = 100
10.times do
	randInt.push(start + rand(ender - start))
end
puts randInt

puts ''
puts 'Sort Random Array with numbers between 55 & 100'
randInt.sort!
puts randInt

puts ''
puts 'Create random string with 5 characters'
letters = alphabet.sort!
newString = ''
5.times do
	randLetter = rand(26);
	newString += alphabet[randLetter]
end
puts newString

puts ''
puts 'Create array with 10 random strings of 5 characters'

randStringArray = []
10.times do
	newString = ''
	5.times do
		randLetter = rand(26)
		newString += alphabet[randLetter]
	end
	randStringArray.push(newString)
end

puts randStringArray 